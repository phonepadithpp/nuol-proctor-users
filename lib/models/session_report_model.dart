// To parse this JSON data, do
//
//     final sessionReportModel = sessionReportModelFromJson(jsonString);

import 'dart:convert';

SessionReportModel sessionReportModelFromJson(String str) =>
    SessionReportModel.fromJson(json.decode(str));

String sessionReportModelToJson(SessionReportModel data) =>
    json.encode(data.toJson());

class SessionReportModel {
  SessionReportModel({
    required this.reportId,
    required this.sessionId,
    required this.detail,
    required this.createdDate,
  });

  final int reportId;
  final int sessionId;
  final String detail;
  final DateTime createdDate;

  factory SessionReportModel.fromJson(Map<String, dynamic> json) =>
      SessionReportModel(
        reportId: json["report_id"],
        sessionId: json["session_id"],
        detail: json["detail"],
        createdDate: DateTime.parse(json["created_date"]),
      );

  Map<String, dynamic> toJson() => {
        "report_id": reportId,
        "session_id": sessionId,
        "detail": detail,
        "created_date": createdDate.toIso8601String(),
      };
}
