// To parse this JSON data, do
//
//     final attendanceModel = attendanceModelFromJson(jsonString);
import 'dart:convert';

AttendanceModel attendanceModelFromJson(String str) =>
    AttendanceModel.fromJson(json.decode(str));

String attendanceModelToJson(AttendanceModel data) =>
    json.encode(data.toJson());

class AttendanceModel {
  AttendanceModel({
    required this.id,
    required this.seatNo,
    required this.fullName,
    required this.birthDate,
    required this.pictureFile,
    required this.mobileNo,
    required this.provinceName,
    required this.schoolName,
  });

  final int id;
  final String seatNo;
  final String fullName;
  final DateTime birthDate;
  final dynamic pictureFile;
  final String mobileNo;
  final dynamic provinceName;
  final String schoolName;

  factory AttendanceModel.fromJson(Map<String, dynamic> json) =>
      AttendanceModel(
        id: json["id"],
        seatNo: json["seat_no"],
        fullName: json["full_name"],
        birthDate: DateTime.parse(json["birth_date"]),
        pictureFile: json["picture_file"] ?? "-",
        mobileNo: json["mobile_no"] ?? "-",
        provinceName: json["province_name"] ?? "-",
        schoolName: json["school_name"] ?? "-",
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "seat_no": seatNo,
        "full_name": fullName,
        "birth_date": birthDate.toIso8601String(),
        "picture_file": pictureFile,
        "mobile_no": mobileNo,
        "province_name": provinceName,
        "school_name": schoolName,
      };
}
