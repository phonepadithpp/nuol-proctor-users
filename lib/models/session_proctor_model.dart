// To parse this JSON data, do
//
//     final sessionProctorModel = sessionProctorModelFromJson(jsonString);

import 'dart:convert';

SessionProctorModel sessionProctorModelFromJson(String str) =>
    SessionProctorModel.fromJson(json.decode(str));

String sessionProctorModelToJson(SessionProctorModel data) =>
    json.encode(data.toJson());

class SessionProctorModel {
  SessionProctorModel({
    required this.sessionId,
    required this.roomId,
    required this.date,
    required this.session,
    required this.status,
    required this.roomName,
    required this.buildingName,
    required this.siteName,
  });

  final int sessionId;
  final int roomId;
  final DateTime date;
  final String session;
  final String status;
  final String roomName;
  final String buildingName;
  final String siteName;

  factory SessionProctorModel.fromJson(Map<String, dynamic> json) =>
      SessionProctorModel(
        sessionId: json["session_id"],
        roomId: json["room_id"],
        date: DateTime.parse(json["date"]),
        session: json["session"],
        status: json["status"],
        roomName: json["room_name"],
        buildingName: json["building_name"],
        siteName: json["site_name"],
      );

  Map<String, dynamic> toJson() => {
        "session_id": sessionId,
        "room_id": roomId,
        "date": date.toIso8601String(),
        "session": session,
        "status": status,
        "room_name": roomName,
        "building_name": buildingName,
        "site_name": siteName,
      };
}
