// To parse this JSON data, do
//
//     final roomProctorModel = roomProctorModelFromJson(jsonString);

import 'dart:convert';

RoomProctorModel roomProctorModelFromJson(String str) =>
    RoomProctorModel.fromJson(json.decode(str));

String roomProctorModelToJson(RoomProctorModel data) =>
    json.encode(data.toJson());

class RoomProctorModel {
  RoomProctorModel({
    required this.proctorId,
    required this.name,
    required this.fromFaculty,
    required this.username,
    required this.password,
    required this.roleId,
    required this.modifiedDate,
  });

  final int proctorId;
  final String name;
  final String fromFaculty;
  final String username;
  final String password;
  final int roleId;
  final DateTime modifiedDate;

  factory RoomProctorModel.fromJson(Map<String, dynamic> json) =>
      RoomProctorModel(
        proctorId: json["proctor_id"],
        name: json["name"],
        fromFaculty: json["from_faculty"],
        username: json["username"],
        password: json["password"],
        roleId: json["role_id"],
        modifiedDate: DateTime.parse(json["modified_date"]),
      );

  Map<String, dynamic> toJson() => {
        "proctor_id": proctorId,
        "name": name,
        "from_faculty": fromFaculty,
        "username": username,
        "password": password,
        "role_id": roleId,
        "modified_date": modifiedDate.toIso8601String(),
      };
}
