// To parse this JSON data, do
//
//     final penaltyModel = penaltyModelFromJson(jsonString);

import 'dart:convert';

PenaltyModel penaltyModelFromJson(String str) =>
    PenaltyModel.fromJson(json.decode(str));

String penaltyModelToJson(PenaltyModel data) => json.encode(data.toJson());

class PenaltyModel {
  PenaltyModel({
    required this.applicantId,
    required this.sessionId,
    required this.fault,
    required this.penalty,
    required this.createdDate,
  });

  final int applicantId;
  final int sessionId;
  final String fault;
  final String penalty;
  final DateTime createdDate;

  factory PenaltyModel.fromJson(Map<String, dynamic> json) => PenaltyModel(
        applicantId: json["applicant_id"],
        sessionId: json["session_id"],
        fault: json["fault"],
        penalty: json["penalty"],
        createdDate: DateTime.parse(json["created_date"]),
      );

  Map<String, dynamic> toJson() => {
        "applicant_id": applicantId,
        "session_id": sessionId,
        "fault": fault,
        "penalty": penalty,
        "created_date":
            "${createdDate.year.toString().padLeft(4, '0')}-${createdDate.month.toString().padLeft(2, '0')}-${createdDate.day.toString().padLeft(2, '0')}",
      };
}
