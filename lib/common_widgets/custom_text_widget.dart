import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  const CustomText({Key? key, required this.text, this.textSize = 18})
      : super(key: key);
  final String text;
  final double textSize;
  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: TextStyle(fontSize: textSize, fontWeight: FontWeight.bold));
  }
}
