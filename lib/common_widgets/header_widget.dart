import 'package:flutter/material.dart';

import '../utils/util.dart';

class HeaderWidget extends StatelessWidget {
  final String title;
  const HeaderWidget({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Text(title,
            style: const TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
        Text(
          AppUtil.getTodayDate(),
          style: const TextStyle(fontSize: 18),
        )
      ],
    );
  }
}
