import 'package:flutter/material.dart';

class CustomDialog extends StatelessWidget {
  final VoidCallback onPressed;
  final String positiveButtonTitle;
  final String title;
  final String content;
  final String negativeButtonTitle;

  const CustomDialog(
      {Key? key,
      required this.onPressed,
      required this.positiveButtonTitle,
      required this.title,
      required this.content,
      required this.negativeButtonTitle})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: <Widget>[
        TextButton(
          child: Text(negativeButtonTitle),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        ElevatedButton(
          child: Text(positiveButtonTitle),
          onPressed: onPressed,
        ),
      ],
    );
  }
}
