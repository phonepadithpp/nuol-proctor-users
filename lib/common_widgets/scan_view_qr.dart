// import 'dart:developer';
// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:qr_code_scanner/qr_code_scanner.dart';

// class ScanViewQR extends StatefulWidget {
//   @override
//   _ScanViewQRState createState() => _ScanViewQRState();
// }

// class _ScanViewQRState extends State<ScanViewQR> {
//   final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
//   Barcode result;
//   QRViewController controller;

//   @override
//   void reassemble() {
//     super.reassemble();
//     if (Platform.isAndroid) {
//       controller.pauseCamera();
//     } else if (Platform.isIOS) {
//       controller.resumeCamera();
//     }
//   }

//   void _onQRViewCreated(QRViewController controller) {
//     this.controller = controller;
//     controller.scannedDataStream.listen((scanData) {
//     //  print(scanData.code); 
//       setState(() {
//         result = scanData;
//       });
//     });
//   }

//   @override
//   void dispose() {
//     controller?.dispose();

//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     // print(result.rawBytes);
//     var scanArea = (MediaQuery.of(context).size.width < 400 ||
//             MediaQuery.of(context).size.height < 400)
//         ? 250.0
//         : 400.0;
//     return Container(
//       child: Column(
//         children: [
//           Expanded(
//             flex: 5,
//             child: QRView(
//               key: qrKey,
//               onQRViewCreated: _onQRViewCreated,
//               overlay: QrScannerOverlayShape(
//                   borderColor: Colors.red,
//                   borderRadius: 10,
//                   borderLength: 30,
//                   borderWidth: 10,
//                   cutOutSize: scanArea),
//               onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
//             ),
//           ),
//           // Expanded(child: Text("ຂໍ້ມູນ: " + result.code?? "")),
//           Expanded(
//               child: ElevatedButton(
//             onPressed: () {},
//             child: Text("ກົດເພື່ອກວດສອບ"),
//           ))
//         ],
//       ),
//     );
//   }

//   void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
//     // log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
//     if (!p) {
//       ScaffoldMessenger.of(context).showSnackBar(
//         SnackBar(content: Text('no Permission')),
//       );
//     }
//   }
// }
