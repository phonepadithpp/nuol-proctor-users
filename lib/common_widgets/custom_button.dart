import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final String? title;
  final Color? color;

  const CustomButton(
      {Key? key,
      required this.onPressed,
      @required this.title,
      this.color = Colors.red})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 42,
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
          onPressed: onPressed,
          child: Text(title!, style: const TextStyle(fontSize: 16))),
    );
  }
}
