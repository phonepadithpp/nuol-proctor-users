import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nuol_exam_committee/common_widgets/custom_button.dart';

import '../utils/constant.dart';
import 'login_view_model.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final GlobalKey<FormState> formKey = GlobalKey();
  final LoginViewModel _viewModel = Get.put(LoginViewModel());

  TextEditingController usernameCtr = TextEditingController();
  TextEditingController passwordCtr = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('ລົງຊື່ເຂົ້າໃຊ້ງານ'),
      // ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    const SizedBox(height: 32),
                    const Text(
                      "ລະບົບລາຍງານການສອບເສັງເຂົ້າ ມຊ ສຳລັບຄະນະກຳມະການ",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: Constants.heading3),
                    ),
                    const SizedBox(height: 24),
                    Image.asset('assets/images/exam.png'),
                    const SizedBox(height: 24),
                    loginForm(),
                    const SizedBox(height: 28),
                    const Text(
                      "ໝາຍເຫດ: ເວັບໄຊນີ້ນຳໃຊ້ໃນໄລຍະທົດສອບລະບົບເທົ່ານັ້ນ",
                      style: TextStyle(fontSize: 24, color: Colors.red),
                    )
                  ],
                ),
                const Text("2022 \u00a9 ມະຫາວິທະຍາໄລແຫ່ງຊາດ")
              ],
            ),
          ),
        ),
      ),
    );
  }

  Form loginForm() {
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: formKey,
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        TextFormField(
          controller: usernameCtr,
          validator: (value) {
            return (value == null || value.isEmpty)
                ? 'Please Enter username'
                : null;
          },
          decoration: inputDecoration('Username', Icons.person),
        ),
        const SizedBox(height: 8),
        TextFormField(
          validator: (value) {
            return (value == null || value.isEmpty)
                ? 'Please Enter Password'
                : null;
          },
          obscureText: true,
          controller: passwordCtr,
          decoration: inputDecoration('Password', Icons.lock),
        ),
        const SizedBox(height: 24),
        SizedBox(
          width: double.infinity,
          height: 42,
          child: CustomButton(
            onPressed: () async {
              if (formKey.currentState?.validate() ?? false) {
                await _viewModel.loginUser(usernameCtr.text, passwordCtr.text);
              }
            },
            title: 'ລົງຊື່ເຂົ້າໃຊ້ງານ',
          ),
        ),

        // TextButton(
        //   onPressed: () {
        //     setState(() {
        //       _formType = FormType.register;
        //     });
        //   },
        //   child: Text('Does not have an account?'),
        // )
      ]),
    );
  }

  Form registerForm() {
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: formKey,
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        TextFormField(
          controller: usernameCtr,
          validator: (value) {
            return (value == null || value.isEmpty)
                ? 'Please Enter username'
                : null;
          },
          decoration: inputDecoration('Username', Icons.person),
        ),
        const SizedBox(
          height: 8,
        ),
        TextFormField(
          validator: (value) {
            return (value == null || value.isEmpty)
                ? 'Please Enter Password'
                : null;
          },
          controller: passwordCtr,
          decoration: inputDecoration('Password', Icons.lock),
        ),
        const SizedBox(
          height: 8,
        ),
        TextFormField(
          validator: (value) {
            return (value == null || value.isEmpty || value != passwordCtr.text)
                ? 'Passwords does not match'
                : null;
          },
          decoration: inputDecoration('Retype Password', Icons.lock),
        ),
        // ElevatedButton(
        //   onPressed: () async {
        //     if (formKey.currentState?.validate() ?? false) {
        //       await _viewModel.registerUser(usernameCtr.text, passwordCtr.text);
        //     }
        //   },
        //   child: Text('Register'),
        // ),
        TextButton(
          onPressed: () {
            setState(() {});
          },
          child: const Text('Login'),
        )
      ]),
    );
  }
}

InputDecoration inputDecoration(String labelText, IconData iconData,
    {String? prefix, String? helperText}) {
  return InputDecoration(
    contentPadding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
    helperText: helperText,
    labelText: labelText,
    labelStyle: const TextStyle(color: Colors.grey),
    fillColor: Colors.grey.shade200,
    filled: true,
    prefixText: prefix,
    prefixIcon: Icon(
      iconData,
      size: 20,
    ),
    prefixIconConstraints: const BoxConstraints(minWidth: 60),
    enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: const BorderSide(color: Colors.black)),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: const BorderSide(color: Colors.black)),
    errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: const BorderSide(color: Colors.black)),
    border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: const BorderSide(color: Colors.black)),
  );
}

enum FormType { login, register }
