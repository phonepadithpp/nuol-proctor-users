import 'dart:convert';

import 'package:get/get_connect.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:nuol_exam_committee/utils/api.dart';

import '../model/login_request_model.dart';
import '../model/login_response_model.dart';
import 'package:http/http.dart' as http;

/// LoginService responsible to communicate with web-server
/// via authenticaton related APIs
class LoginService extends GetConnect {
  // final String loginUrl = 'http://10.0.2.2:8000/api/login';
  final String loginUrl = Api.authUrl!;
  // final String registerUrl = 'https://reqres.in/api/register';

  Future<LoginResponseModel?> fetchLogin(LoginRequestModel model) async {
    // final response =
    //     await post(loginUrl, model.toJson(), headers: <String, String>{
    //   'Content-Type': 'application/json; charset=UTF-8',
    // });
    // print("response from api" + response.toString());
    try {
      final response = await http.post(Uri.parse(loginUrl),
          body: jsonEncode(model.toJson()));
      if (response.statusCode == HttpStatus.ok) {
        // print(response.body);
        final responseData = json.decode(response.body);

        Map<String, dynamic> payload = Jwt.parseJwt(responseData['token']);
        if (payload['role_id'] == 2) {
          return LoginResponseModel.fromJson(responseData);
        } else {
          return null;
        }
        // print(responseData['token']);

      } else {
        return null;
      }
    } catch (e) {
      return null;
    }

    // final response =
    //     await http.post(Uri.parse(loginUrl), body: jsonEncode(model.toJson()));
    // print("response from api " + response.body);
  }

  // Future<RegisterResponseModel?> fetchRegister(
  //     RegisterRequestModel model) async {
  //   final response = await post(registerUrl, model.toJson());

  //   if (response.statusCode == HttpStatus.ok) {
  //     return RegisterResponseModel.fromJson(response.body);
  //   } else {
  //     return null;
  //   }
  // }
}
