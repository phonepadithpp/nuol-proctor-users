import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
// import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:nuol_exam_committee/models/session_proctor_model.dart';

import '../core/cache_manager.dart';
import '../utils/api.dart';
import '../utils/dio_helper.dart';

class SessionProvider extends ChangeNotifier {
  List<SessionProctorModel>? sessionProcData = [];
  bool loading = false;

  int get totalSession => sessionProcData!.length;

  getSessionProc() async {
    loading = true;
    sessionProcData = await fetchAllSessionProc();
    loading = false;
    notifyListeners();
  }
}

Future<List<SessionProctorModel>> fetchAllSessionProc() async {
  final dio = await DioHelper.dioConnection;
  final userData = CacheManager.getUser();
  final user = json.decode(userData!)['data'];
  List<SessionProctorModel>? sessions = <SessionProctorModel>[];
  try {
    final res = await dio.get(
        Api.mainApi! + "/api/sessionproctors/" + user['proctor_id'].toString());
    if (res.statusCode == 200) {
      for (var item in res.data['data']) {
        final session = SessionProctorModel.fromJson(item);
        sessions.add(session);
      }
    } else {
      log("error");
    }
    return sessions;
  } catch (e) {
    log(e.toString());
  }
  return sessions;
}
