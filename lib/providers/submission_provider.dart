import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:nuol_exam_committee/models/session_report_model.dart';
// import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../models/session_proctor_model.dart';
import '../utils/api.dart';
import '../utils/dio_helper.dart';
import '../utils/util.dart';

class SubmissionProvider extends ChangeNotifier {
  bool loading = false;
  SessionReportModel? sessionReportModel;

  addSubmissionExam(SessionProctorModel item, String overall) async {
    loading = true;
    await submissionOnce(item, overall);
    loading = false;
    notifyListeners();
  }

  updateSubmissionExam(
      SessionProctorModel item, String overall, int reportId) async {
    loading = true;
    await updateSubmission(item, overall, reportId);
    loading = false;
    notifyListeners();
  }

  getSubmissionReportExam(SessionProctorModel item) async {
    loading = true;
    sessionReportModel = await fetchSessionReport(item);
    loading = false;
    notifyListeners();
  }
}

Future<String> submissionOnce(SessionProctorModel item, String overall) async {
  final dio = await DioHelper.dioConnection;
  String msg = "";
  try {
    final res = await dio.post(Api.mainApi! + "/api/proctor/report",
        data: jsonEncode({
          "detail": overall,
          "session_id": item.sessionId,
          "created_date": AppUtil.getQueryDate(DateTime.now())
        }));
    if (res.statusCode == 200) {
      msg = res.data['message'];
      // log(msg);
      return msg;
    } else {
      log("error");
    }
    return msg;
  } catch (e) {
    log(e.toString());
  }
  return msg;
}

Future<String> updateSubmission(
    SessionProctorModel item, String overall, int reportId) async {
  final dio = await DioHelper.dioConnection;
  String msg = "";
  // print("update karn laiy ngartn ${item.sessionId}");
  try {
    final res = await dio.put(
        Api.mainApi! + "/api/proctor/report/" + reportId.toString(),
        data: jsonEncode({
          "detail": overall,
          "session_id": item.sessionId,
          "created_date": AppUtil.getQueryDate(DateTime.now())
        }));
    if (res.statusCode == 200) {
      msg = res.data['message'];
      // log(msg);
      return msg;
    } else {
      log("error");
    }
    return msg;
  } catch (e) {
    log(e.toString());
    // print(e);
  }
  return msg;
}

Future<SessionReportModel> fetchSessionReport(SessionProctorModel item) async {
  final dio = await DioHelper.dioConnection;
  SessionReportModel? sessionReport = SessionReportModel(
      reportId: 0000, sessionId: 0000, detail: "", createdDate: DateTime.now());
  try {
    final res = await dio.get(Api.mainApi! +
        "/api/proctor/report/session/" +
        item.sessionId.toString());
    if (res.statusCode == 200) {
      // print(res.data['data'][0]);
      sessionReport = SessionReportModel.fromJson(res.data['data'][0] ?? "");
      // log(sessionReport.detail);
    } else {
      log("error");
    }
    return sessionReport;
  } catch (e) {
    log(e.toString());
  }
  return sessionReport!;
}
