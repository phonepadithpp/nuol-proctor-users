import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:nuol_exam_committee/models/attendance_model.dart';
import 'package:nuol_exam_committee/models/room_proctor_model.dart';
import 'package:nuol_exam_committee/models/session_proctor_model.dart';
import 'package:nuol_exam_committee/utils/util.dart';

import '../core/cache_manager.dart';
import '../models/penalty_model.dart';
import '../utils/api.dart';
import '../utils/dio_helper.dart';

class HomeProvider extends ChangeNotifier {
  List<AttendanceModel>? _attentData = [];
  List<AttendanceModel>? allstudentlist = [];
  List<AttendanceModel>? attentList = [];
  int? studentList = 0;
  List<AttendanceModel>? studentSession = [];
  List<PenaltyModel>? penaltyList = [];
  RoomProctorModel? roomProc;

  bool loading = false;

  int get totalStudent => studentList!;

  List<AttendanceModel>? get totalStudentList => _attentData;

  getAttendees(SessionProctorModel item) async {
    loading = true;
    _attentData = await fetchAllAttendance(item);
    studentList = await fetchNoAttendance(item);
    loading = false;
    notifyListeners();
  }

  createAttendance(AttendanceModel item, SessionProctorModel ss) async {
    await addAttendance(item, ss);
    studentSession = await fetchStudentSession(ss);
    notifyListeners();
  }

  deleteAttendance(AttendanceModel item, SessionProctorModel ss) async {
    await deleteAttendanceSS(item, ss);
    studentSession = await fetchStudentSession(ss);
    notifyListeners();
  }

  deleteAllAttendance(SessionProctorModel ss) async {
    for (var item in studentSession!) {
      await deleteAttendanceSS(item, ss);
    }
    notifyListeners();
  }

  getStudentSession(SessionProctorModel ss) async {
    loading = true;
    studentSession = await fetchStudentSession(ss);
    loading = false;
    notifyListeners();
  }

  addBreakerOnce(PenaltyModel item, SessionProctorModel session) async {
    await addPenalty(item, session);
    notifyListeners();
  }
}

Future<bool> addAttendance(
    AttendanceModel item, SessionProctorModel session) async {
  final dio = await DioHelper.dioConnection;
  // List<AttendanceModel>? attendances = <AttendanceModel>[];
  // print("Application id: " + item.id.toString());
  // print("session id: " + session.sessionId.toString());
  try {
    final res = await dio.post(Api.mainApi! + "/api/proctor/attendance",
        data: jsonEncode({
          "applicant_id": item.id,
          "session_id": session.sessionId,
          "created_date": AppUtil.getQueryDate(DateTime.now())
        }));
    if (res.statusCode == 200) {
      return true;
    } else {
      log("error");
    }
    return true;
  } catch (e) {
    log(e.toString());
  }
  return true;
}

Future<bool> deleteAttendanceSS(
    AttendanceModel item, SessionProctorModel session) async {
  final dio = await DioHelper.dioConnection;
  try {
    final res = await dio.delete(Api.mainApi! + "/api/proctor/attendance",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        }),
        data: jsonEncode(
            {"applicant_id": item.id, "session_id": session.sessionId}));
    if (res.statusCode == 200) {
      return true;
    } else {
      log("error");
    }
    return true;
  } on DioError catch (e) {
    log(e.toString());
  }
  return true;
}

Future<List<AttendanceModel>> fetchAllAttendance(
    SessionProctorModel item) async {
  final dio = await DioHelper.dioConnection;
  List<AttendanceModel>? attendances = <AttendanceModel>[];

  try {
    final res = await dio.get(Api.mainApi! +
        "/api/liststudentsnotinsession/" +
        item.sessionId.toString());
    if (res.statusCode == 200) {
      for (var item in res.data['data']) {
        final attend = AttendanceModel.fromJson(item);
        attendances.add(attend);
      }
    } else {
      log("error");
    }
    return attendances;
  } catch (e) {
    log(e.toString());
  }
  return attendances;
}

Future<int> fetchNoAttendance(SessionProctorModel item) async {
  final dio = await DioHelper.dioConnection;
  // List<AttendanceModel>? attendances = <AttendanceModel>[];
  int countAttend = 0;
  try {
    final res = await dio
        .get(Api.mainApi! + "/api/countstudent/" + item.roomId.toString());
    if (res.statusCode == 200) {
      // for (var item in res.data['data']) {
      //   final attend = AttendanceModel.fromJson(item);
      //   attendances.add(attend);
      // }
      // print(res.data['data'][0]['count']);
      countAttend = res.data['data'][0]['count'];
      return countAttend;
    } else {
      log("error");
    }
  } catch (e) {
    log(e.toString());
  }
  return countAttend;
}

Future<bool> addPenalty(PenaltyModel item, SessionProctorModel session) async {
  final dio = await DioHelper.dioConnection;
  // List<AttendanceModel>? attendances = <AttendanceModel>[];
  try {
    final res = await dio.post(Api.mainApi! + "/api/proctor/rulebreaker",
        options: Options(headers: {
          "Content-Type": "application/json",
        }),
        data: jsonEncode({
          "applicant_id": item.applicantId,
          "session_id": session.sessionId,
          "fault": item.fault,
          "penalty": item.penalty,
          "created_date": AppUtil.getQueryDate(DateTime.now())
        }));
    if (res.statusCode == 200) {
      return true;
    } else {
      log("error");
    }
    return true;
  } catch (e) {
    log(e.toString());
  }
  return true;
}

Future<List<AttendanceModel>> fetchStudentSession(
    SessionProctorModel item) async {
  final dio = await DioHelper.dioConnection;
  List<AttendanceModel>? attendances = <AttendanceModel>[];
  try {
    final res = await dio.get(Api.mainApi! +
        "/api/liststudentinsession/" +
        item.sessionId.toString());
    if (res.statusCode == 200) {
      for (var item in res.data['data']) {
        final attend = AttendanceModel.fromJson(item);
        attendances.add(attend);
      }
    } else {
      log("error");
    }
    return attendances;
  } catch (e) {
    log(e.toString());
  }
  return attendances;
}

Future<RoomProctorModel?> fetchProctorDetail() async {
  final dio = await DioHelper.dioConnection;
  final userData = CacheManager.getUser();
  final user = json.decode(userData!)['data'];
  RoomProctorModel? protoc;
  try {
    final res = await dio.get(
        Api.mainApi! + "/api/roomproctors/" + user['proctor_id'].toString());
    if (res.statusCode == 200) {
      protoc = RoomProctorModel.fromJson(res.data['data']);
    } else {
      log("error");
    }
    return protoc;
  } catch (e) {
    log(e.toString());
  }
  return protoc;
}
