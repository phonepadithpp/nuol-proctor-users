import 'package:flutter/material.dart';
// import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:nuol_exam_committee/providers/home_provider.dart';
import 'package:nuol_exam_committee/providers/submission_provider.dart';
import 'package:nuol_exam_committee/splash_view.dart';
import 'package:provider/provider.dart';

import 'providers/session_provider.dart';

Future main() async {
  // await dotenv.load(fileName: ".env");
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: HomeProvider(),
        ),
        ChangeNotifierProvider.value(
          value: SessionProvider(),
        ),
        ChangeNotifierProvider.value(
          value: SubmissionProvider(),
        ),
      ],
      child: GetMaterialApp(
        title: 'National University of Laos',
        debugShowCheckedModeBanner: false,
        // localizationsDelegates: [
        //   GlobalMaterialLocalizations.delegate,
        //   GlobalWidgetsLocalizations.delegate,
        //   GlobalCupertinoLocalizations.delegate,
        // ],
        supportedLocales: const [
          Locale('en', 'EN'), // English, no country code
          Locale('lo', 'LA'), // Laos, no country code
        ],
        theme: ThemeData(
            primarySwatch: Colors.teal,
            visualDensity: VisualDensity.adaptivePlatformDensity,
            fontFamily: "Bounhome"),
        routes: {
          "/home": (_) => SplashView(),
        },
        home: SplashView(),
      ),
    );
  }
}
