import 'package:get_storage/get_storage.dart';

mixin CacheManager {
  Future<bool> saveToken(String? token) async {
    final box = GetStorage();
    await box.write(CacheManagerKey.token.toString(), token);
    return true;
  }

  Future<void> saveUser(dynamic user) async {
    final box = GetStorage();
    await box.write(CacheManagerKey.user.toString(), user);
    // return true;
  }

  static String? getToken() {
    final box = GetStorage();
    return box.read(CacheManagerKey.token.toString());
  }

  static String? getUser() {
    final box = GetStorage();
    return box.read(CacheManagerKey.user.toString());
  }

  Future<void> removeToken() async {
    final box = GetStorage();
    await box.remove(CacheManagerKey.token.toString());
  }

  Future<void> removeUser() async {
    final box = GetStorage();
    await box.remove(CacheManagerKey.user.toString());
  }
}

enum CacheManagerKey { token, user }
