import 'dart:convert';

// import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:nuol_exam_committee/utils/api.dart';

import '../utils/dio_helper.dart';
import 'cache_manager.dart';

class AuthenticationManager extends GetxController with CacheManager {
  final isLogged = false.obs;

  void logOut() async {
    isLogged.value = false;
    await removeToken();
    await removeUser();
  }

  void login(String? token) async {
    //Token is cached
    await saveToken(token);
    final dio = await DioHelper.dioConnection;
    final res = await dio.get(Api.mainApi! + "/api/proctor/user");
    // print(res.data);
    await saveUser(jsonEncode(res.data));
    isLogged.value = true;
  }

  void checkLoginStatus() {
    final token = CacheManager.getToken();
    if (token != null) {
      isLogged.value = true;
    }
  }
}
