import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nuol_exam_committee/screens/home/session_screen.dart';

import 'core/authentication_manager.dart';
import 'login/login_view.dart';

class OnBoard extends StatelessWidget {
  const OnBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthenticationManager _authManager = Get.find();

    return Obx(() {
      return _authManager.isLogged.value
          ? const SessionScreen()
          : const LoginView();
    });
  }
}
