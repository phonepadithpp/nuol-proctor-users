import 'package:dio/dio.dart';

import '../core/cache_manager.dart';
import 'package:get/get.dart';

class DioHelper extends GetxController with CacheManager {
  static Dio dio = Dio();
  static String? accessToken;

  static Future<Dio> get dioConnection async {
    // accessToken = await SecureStorage.getToken();
    final token = CacheManager.getToken();
    dio.options.headers["Content-Type"] = "application/json";
    dio.options.headers["Authorization"] = "Bearer $token";
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      options.headers['Authorization'] = 'Bearer $token';
      return handler.next(options);
    }, onError: (error, handler) async {
      if (error.response?.statusCode == 403 ||
          error.response?.statusCode == 401) {
        // await refreshToken();
        // return _retry(error.response.requestOptions);
      }
      return handler.next(error);
    }));
    return dio;
  }

  // static Future<void> refreshToken() async {
  //   SharedPreferences _storage = await SharedPreferences.getInstance();
  //   final refreshToken = _storage.getString('refreshToken');
  //   final response = await dio
  //       .post('/app/refresh-token', data: {'refreshToken': refreshToken});
  //   if (response.statusCode == 200) {
  //     accessToken = response.data['accessToken'];
  //   }
  // }

  // static Future<dynamic> _retry(RequestOptions requestOptions) async {
  //   final options = new Options(
  //     method: requestOptions.method,
  //     headers: requestOptions.headers,
  //   );
  //   return dio.request<dynamic>(requestOptions.path,
  //       data: requestOptions.data,
  //       queryParameters: requestOptions.queryParameters,
  //       options: options);
  // }
  // static Future<Dio> get dioConnection async {
  //   String token = await SecureStorage.getToken();
  //   dio.options.headers["Content-Type"] = "application/json";
  //   dio.options.headers["Authorization"] = "Bearer $token";
  //   return dio;
  // }

}
