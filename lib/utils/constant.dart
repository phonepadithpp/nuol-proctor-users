class Constants {
  static const String appName = "NUOL Exam";

  static const double titleFontSize = 20;
  static const double contentFontSize = 20;
  static const double titleFontPadding = 24.0;

  static const double buttonFontSize = 22;

  static const double heading3 = 24;
  static const double heading4 = 22;
  static const double heading6 = 18;
}
