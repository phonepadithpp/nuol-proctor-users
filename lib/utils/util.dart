import 'package:intl/intl.dart';

class AppUtil {
  static String formatDateTime(DateTime dateTime) {
    return DateFormat('dd/MM/yyyy hh:mm:ss').format(dateTime);
  }

  static String formatDate(DateTime dateTime) {
    return DateFormat('dd/MM/yyyy').format(dateTime);
  }

  static String formatMonth(DateTime dateTime) {
    return DateFormat('yyyy/MM').format(dateTime);
  }

  static String formatTime(DateTime dateTime) {
    return DateFormat('hh:mm:ss').format(dateTime);
  }

  static String getTodayDate() {
    return DateFormat('EEEE dd/MM/yyyy', "lo_LA").format(DateTime.now());
  }

  static DateTime getDate(DateTime d) => DateTime(d.year, d.month, d.day);

  static String getCurrencyFormat(double number) {
    return NumberFormat("#,###").format(number);
  }

  static String getQueryDate(DateTime dateTime) {
    return DateFormat('yyyy-MM-dd').format(dateTime);
  }
}
