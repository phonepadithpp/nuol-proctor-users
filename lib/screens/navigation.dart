import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:intl/date_symbol_data_local.dart';
import 'package:nuol_exam_committee/models/session_proctor_model.dart';
import 'package:nuol_exam_committee/screens/user_account/user_account.dart';

import 'home/home_screen.dart';

class NavigationScreen extends StatefulWidget {
  const NavigationScreen({Key? key, required this.item}) : super(key: key);
  final SessionProctorModel item;
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  int _selectedIndex = 0;
  final localStorage = const FlutterSecureStorage();

  List<Widget> _pages = <Widget>[];

  @override
  void initState() {
    initializeDateFormatting();
    _pages = [
      HomeScreen(sessionProctorModel: widget.item),
      const UserAccount()
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('InfraSol Market - POS Parking'),
      // ),
      body: SafeArea(
        child: Center(
          child: _pages.elementAt(_selectedIndex),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 28,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'ໜ້າຫຼັກ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'ບັນຊີຜູ້ໃຊ້ງານ',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
