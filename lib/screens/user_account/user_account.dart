import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nuol_exam_committee/common_widgets/custom_button.dart';

import '../../core/authentication_manager.dart';
import '../../core/cache_manager.dart';

class UserAccount extends StatefulWidget {
  const UserAccount({Key? key}) : super(key: key);

  @override
  State<UserAccount> createState() => _UserAccountState();
}

class _UserAccountState extends State<UserAccount> {
  final AuthenticationManager _authManager = Get.find();
  // ignore: prefer_typing_uninitialized_variables
  var user;

  @override
  Widget build(BuildContext context) {
    final userData = CacheManager.getUser();
    user = json.decode(userData!)['data'];
    return Scaffold(
      appBar: AppBar(title: const Text("ບັນຊີຜູ້ໃຊ້ງານ")),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            Column(
              children: [
                Row(
                  children: [
                    const Text(
                      'ຊື່ ອາຈານ: ',
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      user['name'],
                      style: const TextStyle(fontSize: 20),
                    ),
                  ],
                ),
                Row(
                  children: [
                    const Text(
                      'ຊື່ການເຂົ້າໃຊ້ງານ: ',
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      user['username'],
                      style: const TextStyle(fontSize: 20),
                    ),
                  ],
                ),
                Row(
                  children: [
                    const Text(
                      'ຈາກຄະນະ: ',
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      user['from_faculty'],
                      style: const TextStyle(fontSize: 20),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 28),
            CustomButton(
                onPressed: () async {
                  _authManager.logOut();
                  Navigator.of(context).popUntil((route) => route.isFirst);
                },
                title: 'ອອກຈາກລະບົບ'),
          ],
        ),
      ),
    );
  }
}
