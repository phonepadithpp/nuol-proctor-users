import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:nuol_exam_committee/common_widgets/custom_button.dart';

class SubmissionComplete extends StatefulWidget {
  const SubmissionComplete({Key? key}) : super(key: key);

  @override
  State<SubmissionComplete> createState() => _SubmissionCompleteState();
}

class _SubmissionCompleteState extends State<SubmissionComplete> {
  Future<bool> _onWillPop() async {
    Navigator.of(context).pop(true);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(title: const Text("ການລາຍງານສຳເລັດ")),
        body: Center(
            child: Column(
          children: [
            const FaIcon(FontAwesomeIcons.check, size: 36, color: Colors.green),
            const SizedBox(height: 12),
            const Text(
              "ການລາຍງານສະພາບລວມຫ້ອງສອບເສັງສຳເລັດ",
              style: TextStyle(fontSize: 18),
            ),
            CustomButton(
                onPressed: () {
                  Navigator.of(context).popUntil((route) => route.isFirst);
                },
                title: "ກັບຄືນສູ່ໜ້າຫຼັກ")
          ],
        )),
      ),
    );
  }
}
