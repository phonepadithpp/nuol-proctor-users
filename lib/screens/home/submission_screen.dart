import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:nuol_exam_committee/common_widgets/custom_button.dart';
import 'package:nuol_exam_committee/models/session_proctor_model.dart';
import 'package:nuol_exam_committee/providers/session_provider.dart';
import 'package:nuol_exam_committee/providers/submission_provider.dart';
import 'package:nuol_exam_committee/screens/home/submission_complete.dart';
import 'package:provider/provider.dart';

import '../../core/cache_manager.dart';
import '../../models/attendance_model.dart';
import '../../utils/constant.dart';
import '../../utils/util.dart';

class SubmissionScreen extends StatefulWidget {
  const SubmissionScreen(
      {Key? key,
      required this.sessionData,
      required this.absentData,
      required this.penaltyData})
      : super(key: key);
  final SessionProctorModel sessionData;
  final List<AttendanceModel>? absentData;
  final List<AttendanceModel>? penaltyData;
  @override
  State<SubmissionScreen> createState() => _SubmissionScreenState();
}

class _SubmissionScreenState extends State<SubmissionScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final submissionController = TextEditingController();

  @override
  void dispose() {
    submissionController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    Provider.of<SubmissionProvider>(context, listen: false)
        .getSubmissionReportExam(widget.sessionData);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final userData = CacheManager.getUser();
    final user = json.decode(userData!)['data'];
    final procData = widget.sessionData;
    final sessionReport =
        Provider.of<SubmissionProvider>(context).sessionReportModel;
    if (sessionReport != null) {
      submissionController.text = sessionReport.detail;
    }
    return Scaffold(
      appBar: AppBar(title: const Text("ລາຍງານສະພາບລວມຫ້ອງສອບເສັງ")),
      body: SingleChildScrollView(
        child: Container(
            padding: const EdgeInsets.all(24),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "ຊື່ອາຈານຍາມຫ້ອງເສັງ: ອຈ. " + user['name'],
                      style: const TextStyle(fontSize: Constants.heading6),
                    ),
                    Text(
                      "ສອບເສັງ: " + procData.session,
                      style: const TextStyle(fontSize: Constants.heading6),
                    ),
                    Text(
                      "ຫ້ອງ: " + procData.roomName,
                      style: const TextStyle(fontSize: Constants.heading6),
                    ),
                    Text(
                      "ສູນສອບເສັງ: " +
                          procData.siteName +
                          " ຕຶກ " +
                          procData.buildingName,
                      style: const TextStyle(fontSize: Constants.heading6),
                    ),
                    Text(
                      "ວັນທີສອບເສັງ: " + AppUtil.formatDate(procData.date),
                      style: const TextStyle(fontSize: Constants.heading6),
                    ),
                    const SizedBox(height: 8),
                    const Center(
                      child: Text(
                        "ນັກສຶກສາຂາດສອບເສັງ",
                        style: TextStyle(fontSize: Constants.heading4),
                      ),
                    ),
                    widget.absentData!.isEmpty
                        ? const Center(
                            child: Text("ບໍ່ມີນັກສຶກສາຂາດການສອບເສັງຄັ້ງນີ້"))
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: widget.absentData!.length,
                            itemBuilder: (context, index) {
                              final item = widget.absentData![index];
                              return Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    flex: 3,
                                    child: Text(
                                        "${index + 1}. ຊື່ ນສ: ${item.fullName}"),
                                  ),
                                  Flexible(
                                      flex: 1,
                                      child: Text("ລະຫັດ ນສ: ${item.seatNo}")),
                                ],
                              );
                            },
                          ),
                    const SizedBox(height: 12),
                    const Center(
                      child: Text(
                        "ນັກສຶກສາຜິດລະບຽບ",
                        style: TextStyle(fontSize: Constants.heading4),
                      ),
                    ),
                    widget.penaltyData!.isEmpty
                        ? const Center(child: Text("ບໍ່ມີນັກສຶກສາຜິດລະບຽບ"))
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: widget.penaltyData!.length,
                            itemBuilder: (context, index) {
                              final item = widget.penaltyData![index];
                              return Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                      "${index + 1}. ຊື່ ນສ: ${item.fullName}"),
                                  Text("ລະຫັດ ນສ: ${item.seatNo}"),
                                ],
                              );
                            },
                          )
                  ],
                ),
                const SizedBox(height: 18),
                Form(
                  key: _formKey,
                  child: TextFormField(
                    maxLines: 4, //or null
                    validator: (value) =>
                        value!.isEmpty ? 'ກະລຸນາໃສ່ສະພາບລວມຂອງຫ້ອງເສັງ' : null,
                    controller: submissionController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'ລາຍງານສະພາບລວມຂອງຫ້ອງສອບເສັງ',
                    ),
                  ),
                ),
                const SizedBox(height: 18),
                sessionReport != null && sessionReport.detail != ""
                    ? Center(
                        child: Text(
                          "ຫ້ອງສອບເສັງໄດ້ມີການລາຍງານໄປແລ້ວ ການລາຍງານຄັ້ງນີ້ແມ່ນການອັບເດດການລາຍງານສະພາບລວມ",
                          style: TextStyle(color: Colors.red),
                        ),
                      )
                    : Container(),
                SizedBox(
                  width: double.infinity,
                  child: CustomButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          // print(submissionController.text);
                          if (sessionReport != null &&
                              sessionReport.detail != "") {
                            await Provider.of<SubmissionProvider>(context,
                                    listen: false)
                                .updateSubmissionExam(
                                    widget.sessionData,
                                    submissionController.text,
                                    sessionReport.reportId);
                          } else {
                            await Provider.of<SubmissionProvider>(context,
                                    listen: false)
                                .addSubmissionExam(widget.sessionData,
                                    submissionController.text);
                          }

                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const SubmissionComplete(),
                              ));
                        }
                      },
                      title: sessionReport != null && sessionReport.detail != ""
                          ? "ອັບເດດການລາຍງານສະພາບລວມ"
                          : "ຍືນຍັນການລາຍງານສະພາບລວມຫ້ອງ"),
                )
              ],
            )),
      ),
    );
  }
}
