import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:nuol_exam_committee/models/penalty_model.dart';
import 'package:nuol_exam_committee/models/session_proctor_model.dart';
import 'package:nuol_exam_committee/providers/home_provider.dart';
import 'package:nuol_exam_committee/screens/home/qr_scan.dart';
import 'package:nuol_exam_committee/screens/home/rule_screen.dart';
import 'package:nuol_exam_committee/screens/home/submission_screen.dart';
import 'package:nuol_exam_committee/screens/user_account/user_account.dart';
import 'package:nuol_exam_committee/utils/util.dart';
import 'package:provider/provider.dart';
import '../../common_widgets/custom_button.dart';
import '../../core/cache_manager.dart';
import '../../models/attendance_model.dart';
import '../../utils/constant.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key, required this.sessionProctorModel})
      : super(key: key);
  final SessionProctorModel sessionProctorModel;
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  dynamic user;
  bool isLoading = false;
  List<AttendanceModel>? attentList = [];
  List<AttendanceModel>? studentList = [];
  List<AttendanceModel>? penaltyListTemp = [];
  int? totalStudent;
  // List<AttendanceModel>? _data;
  String? profName;
  String faultValue = 'ກ່າຍຄົນອື່ນ';

  var faultList = [
    'ກ່າຍຄົນອື່ນ',
    'ໃຫ້ຄົນອື່ນກ່າຍ',
    'ກ່າຍເອກະສານທີ່ບໍ່ໄດ້ຮັບອະນຸຍາດ',
    'ນໍາສິ່ງຕ້ອງຫ້າມເຂົ້າຫ້ອງສອບເສັງ',
    'ໃຊ້ອຸປະກອນສື່ສານ',
    "ອື່ນໆ"
  ];

  int penaltyValue = 0;

  @override
  void initState() {
    final homePro = Provider.of<HomeProvider>(context, listen: false);
    homePro.getAttendees(widget.sessionProctorModel);
    homePro.getStudentSession(widget.sessionProctorModel);
    super.initState();
  }

  void onQRScan(String code) {
    var item = studentList!.singleWhere((element) => element.seatNo == code);
    if (item.fullName != "") {
      showAttendAlertDialog(context, item, isAbsent: true);
    }
    // debugPrint(item.fullName);
  }

  Future<bool> _onWillPop(List<AttendanceModel>? item) async {
    if (item!.isNotEmpty) {
      return (await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('ທ່ານໝັ້ນໃຈທີ່ຈະອອກບໍ່?'),
              content: const Text(
                  'ມີຂໍ້ມູນປ່ຽນແປງໜ້ານີ້ ທ່ານຕ້ອງການທີ່ຈະອອກຈາກໜ້ານີ້ບໍ່'),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: const Text('ຍົກເລີກ'),
                ),
                CustomButton(
                  onPressed: () {
                    // Provider.of<HomeProvider>(context, listen: false)
                    //     .deleteAllAttendance(widget.sessionProctorModel);
                    Navigator.of(context).pop(true);
                  },
                  title: 'ຕ້ອງການ',
                ),
              ],
            ),
          )) ??
          false;
    } else {
      Navigator.of(context).pop(true);
      return true;
    }
    // return true;
  }

  @override
  Widget build(BuildContext context) {
    final data = Provider.of<HomeProvider>(context);
    // final data = context.watch<HomeProvider>();
    data.allstudentlist = data.totalStudentList!;
    studentList = data.allstudentlist;
    final userData = CacheManager.getUser();
    user = json.decode(userData!)['data'];
    final procData = widget.sessionProctorModel;
    // totalStudent = data.totalStudent;
    return WillPopScope(
      onWillPop: () => _onWillPop(data.studentSession),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("ຈັດການຫ້ອງເສັງ"),
          centerTitle: true,
          actions: [
            IconButton(
              icon: const Icon(Icons.rule),
              tooltip: 'Rules',
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const RuleScreen()));
              },
            ), // IconButton
            IconButton(
              icon: const Icon(Icons.person),
              tooltip: 'Account',
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const UserAccount()));
              },
            ), // IconButton
          ],
        ),
        body: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 8),
            child: Stack(
              fit: StackFit.expand,
              children: [
                SingleChildScrollView(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "ສະບາຍດີ! ອຈ. " + user['name'],
                      style: const TextStyle(fontSize: Constants.heading4),
                    ),
                    Text(
                      "ອາຈານກຳລັງຍາມການສອບເສັງ: " +
                          procData.session +
                          " ຫ້ອງ: " +
                          procData.roomName,
                      style: const TextStyle(fontSize: 16),
                    ),
                    Text(
                      "ສູນສອບເສັງ " +
                          procData.siteName +
                          " ຕຶກ " +
                          procData.buildingName,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 12),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("ລາຍລະອຽດຫ້ອງສອບເສັງ"),
                        Text(
                          "ວັນທີ " + AppUtil.formatDate(procData.date),
                          style: const TextStyle(fontSize: 20),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: const [
                            Icon(Icons.people),
                            SizedBox(width: 18),
                            Text(
                              "ຈຳນວນນັກສອບເສັງເຂົ້າຫ້ອງສອບ",
                              style: TextStyle(fontSize: 18),
                            ),
                          ],
                        ),
                        data.loading
                            ? Center(
                                child: SpinKitWave(
                                    color: Theme.of(context).primaryColor,
                                    size: 20),
                              )
                            : Text(
                                data.studentSession!.length.toString() +
                                    "/" +
                                    data.totalStudent.toString() +
                                    " ຄົນ",
                                style: const TextStyle(fontSize: 32),
                              ),
                      ],
                    ),
                    const Divider(color: Colors.cyan),
                    _absentStudents(context, data),
                    _attentStudents(context),
                    const SizedBox(height: 150)
                  ],
                )),
                Positioned(
                  bottom: 0,
                  child: Container(
                    color: Colors.white60,
                    width: MediaQuery.of(context).size.width - 33,
                    height: kIsWeb
                        ? MediaQuery.of(context).size.width * 0.1
                        : MediaQuery.of(context).size.width * 0.2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomButton(
                          title: "ລາຍງານການສອບເສັງ",
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SubmissionScreen(
                                    sessionData: widget.sessionProctorModel,
                                    absentData: studentList,
                                    penaltyData: penaltyListTemp,
                                  ),
                                ));
                          },
                        ),
                        const SizedBox(width: 8),
                        SizedBox(
                          height: 42,
                          child: ElevatedButton.icon(
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            QRScanApproval(apFunc: onQRScan)));
                              },
                              icon: const Icon(Icons.qr_code),
                              label: const Text("Scan QR")),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }

  showPenaltyAlertDialog(BuildContext context, AttendanceModel item) {
    // set up the button
    Widget okButton = CustomButton(
      title: "ບັນທຶກ",
      onPressed: () {
        final penaltyItem = PenaltyModel(
            applicantId: item.id,
            sessionId: widget.sessionProctorModel.sessionId,
            fault: faultValue,
            penalty: penaltyValue.toString(),
            createdDate: DateTime.now());
        Provider.of<HomeProvider>(context, listen: false)
            .penaltyList!
            .add(penaltyItem);
        penaltyListTemp!.add(item);
        Provider.of<HomeProvider>(context, listen: false)
            .addBreakerOnce(penaltyItem, widget.sessionProctorModel);
        Navigator.pop(context);
      },
    );
    // set up the button
    Widget cancelButton = TextButton(
      child: const Text("ຍົກເລີກ"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setStateAlert) {
          return AlertDialog(
            title: const Text("ບັນທຶກນັກສຶກສາຜິດລະບຽບ"),
            content: SizedBox(
              height: 250,
              width: 350,
              child: Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text("ຊື່ນັກສຶກສາ: " + item.fullName),
                      Text("ລະຫັດສອບເສັງ: " + item.seatNo),
                      DropdownButton<String>(
                        isExpanded: true,
                        value: faultValue,
                        icon: const Icon(Icons.keyboard_arrow_down),
                        items: faultList.map((String items) {
                          return DropdownMenuItem(
                            value: items,
                            child: Text(items),
                          );
                        }).toList(),
                        onChanged: (String? newValue) {
                          setStateAlert(() {
                            faultValue = newValue!;
                          });
                        },
                      ),
                      DropdownButton<int>(
                        isExpanded: true,
                        value: penaltyValue,
                        icon: const Icon(Icons.keyboard_arrow_down),
                        items: const [
                          DropdownMenuItem(
                            value: 0,
                            child: Text("ກ່າວຕັກເຕືອນ"),
                          ),
                          DropdownMenuItem(
                            value: 1,
                            child: Text('ໃຫ້ຢຸດຕິການສອບເສັງ'),
                          )
                        ],
                        onChanged: (int? newValue) {
                          setStateAlert(() {
                            penaltyValue = newValue!;
                          });
                        },
                      ),
                      const TextField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'ໃສ່ລາຍລະອຽດນັກສຶກສາຜິດລະບຽບ',
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            actions: [cancelButton, okButton],
          );
        });
      },
    );
  }

  showAttendAlertDialog(BuildContext context, AttendanceModel item,
      {bool isAbsent = false}) {
    // set up the button
    Widget okButton = CustomButton(
      title: "ອະນຸຍາດ",
      onPressed: () {
        if (isAbsent) {
          // attentList!.add(item);
          Provider.of<HomeProvider>(context, listen: false)
              .createAttendance(item, widget.sessionProctorModel);
          setState(() {
            studentList!.removeWhere((it) => it.seatNo == item.seatNo);
          });
        } else {
          setState(() {
            studentList!.add(item);
          });
          Provider.of<HomeProvider>(context, listen: false)
              .deleteAttendance(item, widget.sessionProctorModel);
          // attentList!
          //     .removeWhere((it) => it.applicantCode == item.applicantCode);
        }

        Navigator.pop(context);
      },
    );
    // set up the button
    Widget cancelButton = TextButton(
      child: const Text("ຍົກເລີກ"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("ຂໍ້ມູນນັກສອບເສັງ"),
      content: SizedBox(
        height: 250,
        width: 300,
        child: Column(
          children: [
            isAbsent
                ? const Text(
                    "ອະນຸຍາດເຂົ້າຫ້ອງສອບເສັງ?",
                    style: TextStyle(fontSize: Constants.heading6),
                  )
                : Container(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text("ຊື່ນັກສຶກສາ: " + item.fullName),
                Text("ລະຫັດສອບເສັງ: " + item.seatNo),
                Text("ວັນເດືອນປີເກີດ: " + AppUtil.formatDate(item.birthDate)),
                Text("ເບີໂທ: " + (item.mobileNo)),
                Text("ແຂວງ: " + (item.provinceName ?? "")),
                Text("ຊື່ໂຮງຮຽນ: " + item.schoolName),
              ],
            ),
          ],
        ),
      ),
      actions: [cancelButton, okButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _attentStudents(BuildContext context) {
    final stdSession = Provider.of<HomeProvider>(context, listen: false);
    final penaltyList =
        Provider.of<HomeProvider>(context, listen: false).penaltyList;
    return Card(
        child:
            // List AttendanceList = snapshot.data!.data as List;
            Container(
                padding: const EdgeInsets.all(12),
                child: Column(
                  children: [
                    const Text(
                      "ລາຍຊື່ນັກສອບເສັງທັງໝົດ",
                      style: TextStyle(fontSize: Constants.heading6),
                    ),
                    stdSession.loading
                        ? Center(
                            child: SpinKitWave(
                                color: Theme.of(context).primaryColor,
                                size: 20),
                          )
                        : stdSession.studentSession!.isEmpty
                            ? Container(
                                alignment: Alignment.center,
                                width: double.infinity,
                                height: 40,
                                child: const Text(
                                    "ນັກສຶກສາຍັງບໍ່ທັນເຂົ້າຫ້ອງສອບເສັງ"))
                            : LayoutBuilder(builder: (context, constraint) {
                                if (constraint.maxWidth > 800) {
                                  return GridView.count(
                                    // crossAxisCount is the number of columns
                                    shrinkWrap: true,
                                    childAspectRatio: 15,
                                    crossAxisCount: 3,
                                    // This creates two columns with two items in each column
                                    children: List.generate(
                                        stdSession.studentSession!.length,
                                        (index) {
                                      var item =
                                          stdSession.studentSession![index];
                                      return Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Flexible(
                                              flex: 4,
                                              fit: FlexFit.tight,
                                              child: Text(
                                                  "${index + 1}. ${item.fullName}")),
                                          Flexible(
                                              flex: 1,
                                              fit: FlexFit.tight,
                                              child:
                                                  Text(item.seatNo.toString())),
                                          Flexible(
                                            flex: 1,
                                            child: IconButton(
                                                onPressed: () =>
                                                    showAttendAlertDialog(
                                                        context, item,
                                                        isAbsent: true),
                                                icon: const FaIcon(
                                                  FontAwesomeIcons.minus,
                                                  size: 18,
                                                )),
                                          )
                                        ],
                                      );
                                    }),
                                  );
                                } else {
                                  return ListView.builder(
                                    shrinkWrap: true,
                                    itemCount:
                                        stdSession.studentSession!.length,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemBuilder: (context, index) {
                                      var item =
                                          stdSession.studentSession![index];
                                      return Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Flexible(
                                            flex: 5,
                                            child: InkWell(
                                              onTap: () {
                                                showPenaltyAlertDialog(
                                                    context, item);
                                              },
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    "${index + 1}. ${item.fullName}",
                                                    style: TextStyle(
                                                        color: (penaltyList!
                                                                .where((element) =>
                                                                    element
                                                                        .applicantId ==
                                                                    item.id)
                                                                .toList()
                                                                .isNotEmpty)
                                                            ? Colors.red
                                                            : Colors.black),
                                                  ),
                                                  Text(item.seatNo.toString()),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Flexible(
                                            flex: 1,
                                            child: IconButton(
                                                onPressed: () =>
                                                    showAttendAlertDialog(
                                                        context, item),
                                                icon: const FaIcon(
                                                  FontAwesomeIcons.minus,
                                                  size: 18,
                                                )),
                                          )
                                        ],
                                      );
                                    },
                                  );
                                }
                              }),
                  ],
                )));
  }

  Widget _absentStudents(BuildContext context, HomeProvider data) {
    return Card(
        child:
            // List AttendanceList = snapshot.data!.data as List;
            Container(
                padding: const EdgeInsets.all(12),
                child: Column(
                  children: [
                    const Text(
                      "ລາຍຊື່ນັກສອບເສັງທັງໝົດ",
                      style: TextStyle(fontSize: Constants.heading6),
                    ),
                    data.loading
                        ? Center(
                            child: SpinKitWave(
                                color: Theme.of(context).primaryColor,
                                size: 20),
                          )
                        : studentList!.isEmpty
                            ? Container(
                                alignment: Alignment.center,
                                width: double.infinity,
                                height: 40,
                                child: const Text("ນັກສຶກສາມາຄົບຕາມຈຳນວນແລ້ວ"))
                            : LayoutBuilder(builder: (context, constrints) {
                                if (constrints.maxWidth > 800) {
                                  return GridView.count(
                                    // crossAxisCount is the number of columns
                                    shrinkWrap: true,
                                    childAspectRatio: 15,
                                    crossAxisCount: 3,
                                    // This creates two columns with two items in each column
                                    children: List.generate(studentList!.length,
                                        (index) {
                                      var item = studentList![index];
                                      return Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Flexible(
                                              flex: 4,
                                              fit: FlexFit.tight,
                                              child: Text(
                                                  "${index + 1}. ${item.fullName}")),
                                          Flexible(
                                              flex: 1,
                                              fit: FlexFit.tight,
                                              child:
                                                  Text(item.seatNo.toString())),
                                          Flexible(
                                            flex: 1,
                                            child: IconButton(
                                                onPressed: () =>
                                                    showAttendAlertDialog(
                                                        context, item,
                                                        isAbsent: true),
                                                icon: const FaIcon(
                                                  FontAwesomeIcons.plus,
                                                  size: 18,
                                                )),
                                          )
                                        ],
                                      );
                                    }),
                                  );
                                } else {}
                                return ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: studentList!.length,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    var item = studentList![index];
                                    return Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                            flex: 4,
                                            fit: FlexFit.tight,
                                            child: Text(
                                                "${index + 1}. ${item.fullName}")),
                                        Flexible(
                                            flex: 1,
                                            fit: FlexFit.tight,
                                            child:
                                                Text(item.seatNo.toString())),
                                        Flexible(
                                          flex: 1,
                                          child: IconButton(
                                              onPressed: () =>
                                                  showAttendAlertDialog(
                                                      context, item,
                                                      isAbsent: true),
                                              icon: const FaIcon(
                                                FontAwesomeIcons.plus,
                                                size: 18,
                                              )),
                                        )
                                      ],
                                    );
                                  },
                                );
                              }),
                  ],
                )));
  }
}
