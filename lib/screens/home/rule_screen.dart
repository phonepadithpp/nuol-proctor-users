import 'dart:math';
import 'package:flutter/material.dart';
import 'package:webviewx/webviewx.dart';

class RuleScreen extends StatefulWidget {
  const RuleScreen({Key? key}) : super(key: key);

  @override
  State<RuleScreen> createState() => _RuleScreenState();
}

class _RuleScreenState extends State<RuleScreen> {
  late WebViewXController webviewController;
  Size get screenSize => MediaQuery.of(context).size;

  @override
  void dispose() {
    webviewController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // webviewController.loadContent(, sourceType)
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("ກົດລະບຽບການສອບເສັງ")),
      body: Center(
          child: WebViewX(
        initialContent: "https://nuol.edu.la/index.php/lo",
        initialSourceType: SourceType.url,
        height: screenSize.height * 0.95,
        width: min(screenSize.width * 0.95, 1024),
        onWebViewCreated: (controller) => webviewController = controller,
      )),
    );
  }
}
