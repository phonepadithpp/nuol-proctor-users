import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

class QRScanApproval extends StatefulWidget {
  const QRScanApproval({Key? key, required this.apFunc}) : super(key: key);
  final Function apFunc;

  @override
  State<QRScanApproval> createState() => _QRScanApprovalState();
}

class _QRScanApprovalState extends State<QRScanApproval> {
  MobileScannerController cameraController = MobileScannerController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("QR Scan"),
        // actions: [
        //   IconButton(
        //     color: Colors.white,
        //     icon: ValueListenableBuilder(
        //       valueListenable: cameraController.torchState,
        //       builder: (context, state, child) {
        //         switch (state as TorchState) {
        //           case TorchState.off:
        //             return const Icon(Icons.flash_off, color: Colors.grey);
        //           case TorchState.on:
        //             return const Icon(Icons.flash_on, color: Colors.yellow);
        //         }
        //       },
        //     ),
        //     iconSize: 32.0,
        //     onPressed: () => cameraController.toggleTorch(),
        //   ),
        // ],
      ),
      body: MobileScanner(
          allowDuplicates: false,
          controller: cameraController,
          onDetect: (barcode, args) {
            if (barcode.rawValue == null) {
              debugPrint('Failed to scan Barcode');
            } else {
              final String code = barcode.rawValue!;
              debugPrint('Barcode found! $code');
              Navigator.pop(context);
              widget.apFunc(code);
            }
          }),
    );
  }
}
