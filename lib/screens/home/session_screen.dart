import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nuol_exam_committee/models/session_proctor_model.dart';
import 'package:nuol_exam_committee/providers/session_provider.dart';
import 'package:nuol_exam_committee/screens/home/home_screen.dart';
import 'package:nuol_exam_committee/utils/util.dart';
import 'package:provider/provider.dart';
import 'package:get/get.dart';

import '../../core/authentication_manager.dart';
import '../../core/cache_manager.dart';

class SessionScreen extends StatefulWidget {
  const SessionScreen({Key? key}) : super(key: key);

  @override
  State<SessionScreen> createState() => _SessionScreenState();
}

class _SessionScreenState extends State<SessionScreen> {
  final AuthenticationManager _authManager = Get.find();
  @override
  void initState() {
    Provider.of<SessionProvider>(context, listen: false).getSessionProc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final sessionData = Provider.of<SessionProvider>(context);
    final data = sessionData.sessionProcData;
    final userData = CacheManager.getUser();
    final user = json.decode(userData!)['data'];
    // print(data);
    return Scaffold(
      appBar: AppBar(
        title: const Text("ເລືອກຫ້ອງສອບເສັງ"),
        actions: [
          IconButton(
              onPressed: () {
                _authManager.logOut();
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
              icon: const Icon(Icons.logout))
        ],
      ),
      body: SingleChildScrollView(
        child: user['role_id'] == 1
            ? _contentAdmin()
            : Container(
                padding: const EdgeInsets.all(18),
                alignment: AlignmentDirectional.center,
                child: sessionData.loading
                    ? const Center(
                        child: SpinKitChasingDots(color: Colors.teal, size: 40),
                      )
                    : Column(
                        children: [
                          const Text(
                            "ລາຍການ ຫ້ອງສອບເສັງ",
                            style: TextStyle(fontSize: 22),
                          ),
                          Text("ສຳລັບອາຈານ " + user['name']),
                          const SizedBox(
                            height: 32,
                          ),
                          data!.isEmpty
                              ? Text(
                                  "ບໍ່ພົບຫ້ອງສອບເສັງທີຂຶ້ນກັບທ່ານ ກະລຸນາແຈ້ງສູນສອບເສັງຂອງທ່ານ.")
                              : Column(
                                  children: [
                                    const Text(
                                        "ກະລຸນາເລືອກສູນສອບເສັງທີ່ກຳລັງຈະຍາມ"),
                                    ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: data.length,
                                      itemBuilder: (context, index) {
                                        final item = data[index];
                                        return buildContent(item);
                                      },
                                    ),
                                  ],
                                ),
                        ],
                      ),
              ),
      ),
    );
  }

  Widget _contentAdmin() {
    return Container(
      padding: const EdgeInsets.all(12),
      child: Text(
          "ສຳລັບຜູ້ໃຊ້ງານຂອງຜູ້ດູແລລະບົບ ກະລຸນາເຂົ້າໃຊ້ຜ່ານລະບົບຈັດການຈອງ Admin"),
    );
  }

  Widget buildContent(SessionProctorModel item) {
    return Card(
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => HomeScreen(sessionProctorModel: item)));
        },
        child: Container(
          padding: const EdgeInsets.all(18),
          child: Column(
            children: [
              Row(children: [
                const Text("ສອບເສັງ: "),
                Text(
                  item.session,
                  style: const TextStyle(
                      fontSize: 22, fontWeight: FontWeight.bold),
                )
              ]),
              Row(children: [
                const Text("ຫ້ອງສອບເສັງ: "),
                Text(item.roomName,
                    style: const TextStyle(
                        fontSize: 22, fontWeight: FontWeight.bold))
              ]),
              Row(children: [
                const Text("ວັນທີສອບເສັງ: "),
                Text(AppUtil.formatDate(item.date),
                    style: const TextStyle(
                        fontSize: 22, fontWeight: FontWeight.bold))
              ]),
              Row(children: [const Text("ສະຖານະ: "), Text(item.status)]),
              Row(
                children: [const Text("ຊື່ຕຶກ: "), Text(item.buildingName)],
              ),
              Row(
                children: [const Text("ຊື່ສູນສອບເສັງ: "), Text(item.siteName)],
              )
            ],
          ),
        ),
      ),
    );
  }
}
